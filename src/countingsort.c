/*
 * countingsort.c
 *
 *  Created on: 20 Mar 2019
 *      Author: ray
 *
 *
 *
 *
 *
 */

#include<stdio.h>

void printarray (int incount,int myarray[]);

int main() {

	int i;

	printf("countingsort\n");
	int asize = 10;
	int myarray[10] = {7,9,4,2,8,0,1,5,3,6};

	printarray(10,myarray);

	int mycarray[10] = {0,0,0,0,0,0,0,0,0,0}; // array to count occurances of number
	int mysarray[10] = {0,0,0,0,0,0,0,0,0,0}; // array to store sorted items

	printf("\nC array loading\n");
	for (i=0;i<asize;i++) {
		mycarray[myarray[i]]++;
	}
	printarray(10,mycarray);


	printf("\nC array adding\n");
	for (i=1;i<asize;i++) {
		mycarray[i] = mycarray[i]+mycarray[i-1];
	}
	printarray(10,mycarray);


	printf("\nC array shifting\n");
	for (i=asize;i>0;i--) {
		mycarray[i] = mycarray[i-1];
	}
	mycarray[0] = 0;
	printarray(10,mycarray);


	printf("\nS array sorting\n");
	for (i=0;i<asize;i++) {
			mysarray[mycarray[myarray[i]]] = myarray[i];
	}
	mycarray[0] = 0;
	printarray(10,mysarray);

	printf("\nSaving sorted array\n");
	for (i=0;i<asize;i++) {
			myarray[i] = mysarray[i];
	}
	printarray(10,myarray);

	return 0;
}


void printarray (int incount,int myarray[]) {
	int id;

	   for (id=0;id<incount;id++) {
	   		   printf("%d\t",myarray[id]);
	   }
	   printf("\n");
}
