/*
 * simple-bubblesort.c
 *
 *  Created on: 18 Mar 2019
 *      Author: ray
 */

#include<stdio.h>

void printarray(int inarray[]);

int main() {

	int myarray[10] = {7,9,4,2,8,0,1,5,3,6};
	//int myarray[10] = {0,1,2,3,4,5,6,7,8,9};
	//int myarray[10] = {9,8,7,6,5,4,3,2,1,0};

	printarray(myarray);
	printf("\n");

	// begin bubble sort

	// loop variable
	int i;

	// swap temp integer
	int tempi;

	// set hasswapped to start sort
	int hasswapped = 1;

	// while something has been swapped do parse array
	while (hasswapped == 1) {

		// reset hasswapped to check for swapped values
		hasswapped = 0;

		// main parsing loop
		for (i=0;i<(10-1);i++) {
			printarray(myarray);


			if (myarray[i] > myarray[i+1]) {
				tempi = myarray[i];
				myarray[i] = myarray[i+1];
				myarray[i+1] = tempi;
				hasswapped = 1;
				printarray(myarray);
				}
			}
		printf("\n");
	}

	// end sort

	printf("\n");
	printarray(myarray);
	printf("Bubble sort finished\n");

	return 0;
}

void printarray(int inarray[]) {
	int i;
	for (i=0;i<10;i++) {
		printf("%d\t",inarray[i]);
	}
	printf("\n");
}
