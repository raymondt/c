/*
 * mergesort.c
 *
 *  Created on: 19 Mar 2019
 *      Author: ray
 */
#include <stdio.h>


void printarray(int insize, int inarray[]);
int sortmerge(int startloc, int endloc,int step, int * inmyarray);


int main() {

	printf("mergesort\n");
	//int asize = 10;
	int myarray[10] = {7,9,4,2,8,0,1,5,3,6};
	int * pmyarray = myarray;
	printarray(10,myarray);
	printf("\n");

	sortmerge(0,9,1, pmyarray);

	printarray(10,myarray);

	return 0;
}

int sortmerge(int startloc, int endloc,int step, int * inmyarray) {

	printf("############################################################\n");
	printf("starting sortmerge with step %d or %d block\n",step,step*2);



	int mywidth = endloc - startloc +1;
	printf("width = %d\n",mywidth);

	if (step * 2 > mywidth) {
		printf("test Max reached at step %d \n",step*2);
		return 0;
	}

	int rem = mywidth % (step*2); //
	printf("rem = %d\n",rem);

	if (rem != 0) {
		// array not divisable, chop remainder to make divisable and recurse
		sortmerge(startloc,endloc-rem,step, inmyarray);
		printf(">>>>>>>>>>>>>>>>>>>recombine remainder<<<<<<<<<<<<<<<<<<\n");

		int i;
		int arraya[mywidth-rem];
		printf("finishing array1 size %d\n",mywidth-rem);
		int arrayb[rem];
		printf("finishing array2 size %d\n",step);

		for (i=0;i<mywidth-rem;i++) {
			arraya[i] = inmyarray[i];
		}

		for (i=0;i<rem;i++) {

			arrayb[i] = inmyarray[i+mywidth-rem];
		}

		printf("finish arraya\n");
		printarray(mywidth-rem,arraya);
		printf("finish arrayb\n");
		printarray(rem,arrayb);

		// ready for merge
		int ia = 0;
		int ib = 0;

		printf("test if both arrays alive\n");
		while (ia < (mywidth-rem) && ib < (rem)) {
			printf("A=%d B=%d\n",arraya[ia],arrayb[ib]);

			if (arraya[ia] <= arrayb[ib]) {
				inmyarray[ia+ib] = arraya[ia];
				ia++;
			} else {
				inmyarray[ia+ib] = arrayb[ib];
				ib++;
			}
			printarray(ia+ib,inmyarray);
		}
		printf("test if array a is alive\n");
		while (ia < (mywidth-rem)) {
			inmyarray[ia+ib] = arraya[ia];
			ia++;
			printarray(ia+ib,inmyarray);
		}
		printf("test if array b is alive\n");
		while (ib < (rem)) {
			inmyarray[ia+ib] = arrayb[ib];
			ib++;
			printarray(ia+ib,inmyarray);
		}
	} else {

		// ACTUAL SORT START

		int i;
		int arraya[step];
		printf("arrayA size %d\n",step);
		int arrayb[step];
		printf("arrayB size %d\n",step);

		for (i=startloc;i<mywidth-1/step;i=i+(step*2)) {

			printf("i = %d\n",i);
			int ii;
			int ia = 0;
			int ib = 0;

			for (ii=0;ii<step;ii++) {
				arraya[ia] = inmyarray[i+ii];
				ia++;
			}

			for (ii=0;ii<step;ii++) {
				arrayb[ib] = inmyarray[i+step+ii];
				ib++;
			}

			printf("arraya\n");
			printarray(step,arraya);
			printf("arrayb\n");
			printarray(step,arrayb);

			// ready for merge
			ia = 0;
			ib = 0;

			printf("test both loops alive\n");
			while (ia < (step) && ib < (step)) {
				// loops while both arrays are alive.
				if (arraya[ia] <= arrayb[ib]) {
					inmyarray[i+ia+ib] = arraya[ia];
					ia++;
				} else if (arraya[ia] > arrayb[ib]) {
					inmyarray[i+ia+ib] = arrayb[ib];
					ib++;
				}
				printarray(ia+ib+i,inmyarray);
			}
			printf("test loops a alive\n");
			while (ia < (step)) {
				inmyarray[i+ia+ib] = arraya[ia];
				ia++;
				printarray(ia+ib+i,inmyarray);
			}

			printf("test loops b alive\n");
			while (ib < (step)) {
				inmyarray[i+ia+ib] = arrayb[ib];
				ib++;
				printarray(ia+ib+i,inmyarray);
				}

		}

		sortmerge(startloc,endloc,step*2, inmyarray);
	}
	printf("End of step %d\n",step);

	return 0;
}

void printarray(int insize,int inarray[]) {
	int i;
	for (i=0;i<insize;i++) {
		printf("%d\t",inarray[i]);
	}
	printf("\n");
}


