/*
 * 	bubblesort1.c
 *
 *  Created on: 15 Mar 2019
 *      Author: ray
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int sinksort(char isen,int asize, int amax, int ashow);
double mypass = 0;
double mycomp = 0;
double myswap = 0;


int main () {

   srand((unsigned) time(0));
   int maxsize = 10;
   int arraysize = 40;

   printf("sinksort example.\n\n");

   printf("Enter size of the array (max 2147483647)\n");
   scanf("%d",&arraysize);
   if (arraysize > 2147483647) arraysize = 2147483647;

   printf("Enter max size of each item (max 2147483647)\n");
   scanf("%d",&maxsize);
   if (maxsize > 2147483647) maxsize = 2147483647;

   char myc[255];
   char * pmyc = myc;
   printf("Enter Senario:\n1 for random\n2 for best-case\n3 for worst-case\n");
   scanf("%s",pmyc);
   char c = myc[0];
   sinksort(c,arraysize,maxsize,1);


   printf("mypass = %g\n",mypass);
   printf("mycomp = %g\n",mycomp);
   printf("myswap = %g\n",myswap);

   printf("\n\nend of line.");

   return(0);
}

int sinksort(char isen,int asize, int amax, int ashow) {

	   int myarray[asize];
	   int i;


	   if (isen == '1') {
		   //int ic = 2;
		   printf("Random Array\n");
		   for (i=0;i<asize;i++) {
			   myarray[i] = (rand()+amax) % amax;
		   }
		   printf("\n");
	   } else if (isen == '2') {
		   int ic = 0;
		   printf("Best-case Array\n");
		   for (i=0;i<asize;i++) {
			   ic = ic + 1;
			   myarray[i] = ic;
		   }
	   } else {
		   int ic = 2;
		   printf("Worst-case Array\n");
		   for (i=asize;i>-1;i--) {
			   ic = ic + 1;
			   myarray[i] = ic;
		   }
	   }

	   // sinksort start

	   int ib = 1;

	   int templ;
	   mypass++;
	   for (i=0;i<asize;i++) {
		   myswap++;
		   templ = i;
		   mypass++;
		   for (ib=i;ib<asize;ib++) {
			   mycomp++;
			   if (myarray[ib] < myarray[templ]) {
				   myswap++;
				   templ = ib;
			   }
		   }
		   myswap++;
		   int tempi = myarray[templ];
		   myswap++;
		   myarray[templ] = myarray[i];
		   myswap++;
		   myarray[i] = tempi;
		   int ic;
		   for (ic=0;ic<asize;ic++) {
		  			   printf("%d,",myarray[ic]);
		  		   }
		  		   printf("\n");
	   }

	   // sink sort end

return 0;
}
